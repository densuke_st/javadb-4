import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Sample00 とりあえず基本の流れ
 * 
 * 例によって、インメモリDBであることを前提とした手抜きをしています。
 * SQL操作におけるエラーハンドリングを別メソッドに追い出している(一種のリファクタリング)ところも一応チェックです
 * 
 * @author densuke
 *
 */
public class Sample00 {

	public static void errorHandler(Exception e, String message, ResultSet rs, PreparedStatement ps, Connection conn) {
		System.err.println(message);
		e.printStackTrace(System.err);
		try {
			if(rs != null) {
				rs.close();
			}
		} catch(SQLException er) {
			// THRU
		}
		try {
			if(ps != null) {
				ps.close();
			}
		} catch(SQLException er) {
			// THRU
		}
		try {
			if(conn != null) {
				conn.close();
			}
		} catch(SQLException er) {
			// THRU
		}
		System.exit(1);
	}
	
	public static void main(String[] args) {
		final String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		final String dbURL = "jdbc:derby:memory:sample01;create=true";
		final Integer COUNT = 10000;

		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(dbURL);
		} catch(ClassNotFoundException e) {
			errorHandler(e, "ドライバが見つかりません", rs, ps, conn);
		} catch(SQLException e) {
			errorHandler(e, "データベース接続に失敗しました。 データベースURL→" + dbURL, rs, ps, conn);
		}
		
		try {
			ps = conn.prepareStatement("CREATE TABLE fuga(id INTEGER PRIMARY KEY, str VARCHAR(8))");
			Boolean result = ps.execute();
			System.out.println("DEBUG: CREATEの戻り値:" + result);
			ps.close();
		} catch (SQLException e) {
			errorHandler(e, "テーブル作成処理にて例外が発生しました: " + e.getMessage(), rs, ps, conn);
		}
		
		Integer i = 0;
		try {
			ps = conn.prepareStatement("INSERT INTO fuga VALUES(?, ?)");
			System.out.println("初期データを作成しています、お待ちください");
			for(i = 1; i <= COUNT; i++ ) {
				ps.setInt(1, i);
				ps.setString(2, String.valueOf(i % 97));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch(SQLException e) {
			errorHandler(e, "テーブル作成処理にて例外が発生しました: " + e.getMessage(), rs, ps, conn);
		}
		
		// ここからが本題です
		
		try {
			// Derbyでは、剰余演算については関数MODを使います
			ps = conn.prepareStatement("SELECT id, str FROM fuga WHERE mod(id,100) = 0");
			Boolean result = ps.execute();
			System.out.println("DEBUG: SELECTの戻り値:" + result);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
